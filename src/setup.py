import os
from setuptools import find_packages, setup

README = 'Coming Soon'

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='logit',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    license='BSD License',
    description='Client for logging to logapi20 API',
    long_description=README,
    url='gitlab.com',
    author='Nana Duah',
    install_requires=[
        'requests',
        'apscheduler',
        'gevent',
        
    ],
)