import sys
import logging
import requests
import traceback

import queue, threading

from django.views.debug import ExceptionReporter

logger = logging.getLogger(__name__)


class LogLevel:
    INFO = "info"
    WARNING = "warn"
    ERROR = "error"
    CRITICAL = "critical"


class LogitClient:
    def __init__(self, api_access_token: str, api_api_endpint: str, env: str = "prod"):
        self.api_access_token = api_access_token
        self.api_api_endpint = api_api_endpint
        self.env = env
        self.queue = queue.SimpleQueue()

        def log_writer():
            while True:
                log_item = self.queue.get()
                try:
                    requests.post(
                        url=f"{self.api_api_endpint}/api/logs/",
                        data=log_item,
                        headers={"Authorization": f"Bearer {self.api_access_token}"},
                        timeout=5,
                    )

                except Exception as ex:
                    print(f"POST to logapi20 failed! Logger. {ex}")

                if self.env != "prod":
                    print(f'[{log_item["level"]}]: {log_item["message"]}')

        threading.Thread(target=log_writer, daemon=True).start()

    def log(self, message: str, level: str, detail: str = None, tags: list = None):
        if message and level:
            if len(message) > 256:
                detail = f"{detail} {message}"
            if not tags:
                tags = [self.env]
            else:
                tags.append(self.env)
            
            print(f'\n\nLOGGING {message}\n\nTAGS: {tags}')
            self.queue.put(
                {
                    "message": message[:256],
                    "detail": detail,
                    "level": level,
                    "tags": tags,
                }
            )

        logger.debug(f"{message} DETAIL: {detail}")

    def info(self, message: str, detail: str = None, tags: list = None):
        self.log(message=message, detail=detail, tags=tags, level=LogLevel.INFO)

    def warn(self, message: str, detail: str = None, tags: list = None):
        self.log(message=message, detail=detail, tags=tags, level=LogLevel.WARNING)

    def error(self, message: str, detail: str = None, tags: list = None):
        self.log(message=message, detail=detail, tags=tags, level=LogLevel.ERROR)

    def critical(self, message: str, detail: str = None, tags: list = None):
        self.log(message=message, detail=detail, tags=tags, level=LogLevel.CRITICAL)

    def fatal(self, message: str, detail: str = None, tags: list = None):
        self.log(message=message, detail=detail, tags=tags, level=LogLevel.CRITICAL)

    def exception(self, exception: Exception, detail: str = None, tags: list = None):
        detail = traceback.format_exc()
        self.log(message=str(exception), detail=detail, tags=tags, level=LogLevel.ERROR)

    def exception(self, ex: Exception):
        self.log(message='Runtime Error!', detail=f"{ex}",level=LogLevel.ERROR)
