import traceback
from django.conf import settings
from django.http import HttpResponseNotFound
from django.views.debug import ExceptionReporter


logger = settings.LOGGER

REPORT_400_RESPONSE_URLS = settings.CONFIG_PROVIDER.get(
    "REPORT_400_RESPONSE_URLS", ""
).split(",")


class ExceptionMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """
        IMPORTANT!
        If self.is_in_violation is True, abort the request immediately
        and inform admin
        """

        is_static_file = (
            settings.MEDIA_URL in request.path or settings.STATIC_URL in request.path
        )
        is_admin = "/admin/" in request.path

        if not is_static_file and self.is_in_violation(request):

            logger.warn("Accesss violation identified")

            """
            Return page not found result
            """
            return HttpResponseNotFound()

        incedent = None
        response = None
        response = self.get_response(request)

        # Perform post request activities
        is_test_env = settings.ENVIRONMENT == "test"
        if (
            not is_test_env
            and not is_static_file
            and (
                settings.CONFIG_PROVIDER.get("LOG_ADMIN_REQUESTS", "False") == "True"
                or not is_admin
            )
        ):
            self.after_view_rendered(request, response, incedent=incedent)
        return response
        
    def process_exception(self,request, exception):
        reporter = ExceptionReporter(request=request, exc_type=None,tb=None, is_email=True, exc_value=exception)
        logger.error(f'{exception}', detail=reporter.get_traceback_text())

    def after_view_rendered(self, request, response, incedent=None):

        self.log_request(request, response, incedent=incedent)

        return response

    def log_request(self, request, response, incedent=None):
        url = request.path
        ip_address = request.META.get("REMOTE_ADDR", None)
        x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR", None)
        method = request.META.get("REQUEST_METHOD", "GET")
        user_agent = request.META.get("HTTP_USER_AGENT", "Unknown")
        referrer = request.META.get("HTTP_REFERER", None)
        if x_forwarded_for:
            ip_address = x_forwarded_for.split(",")[0]

        user = None
        application = None

        if request.user and request.user.is_authenticated:
            user = request.user

        if getattr(request, "auth", None) and getattr(
            request.auth, "application", None
        ):
            application = request.auth.application

        data = self.get_request_data(request)
        incedent_message = (
            incedent["message"] if incedent and "message" in incedent else ""
        )
        log_data = {
            "message": f"{method.upper()}: {url} {response.status_code}",
            "detail": f"{response.content}<br/>{incedent_message} DATA: {data} IP: {ip_address} USER: {user} APP: {application} REFERRER: {referrer} USER AGENT: {user_agent} ",
            "tags": [ip_address, method, response.status_code],
        }

        if response.status_code in range(400, 500):
            logger.warn(**log_data)
        else:
            logger.info(**log_data)


    def is_in_violation(self, request):
        return False

    def get_request_data(self, request):
        # data=[]

        # if request.POST:
        #     data=[
        #         f"{k}={v}" for k,v in request.POST.items()
        #     ]

        return request.POST or request.GET
